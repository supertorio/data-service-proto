(function() {

    'use strict';

    var app = angular.module('serviceProto', [
        'Beam.Data',
        'Beam.Sample'
    ]);


    app.controller('appController', ['$scope', 'ApiDataService', 'STORAGE_TYPES',
        function($scope, ApiDataService, STORAGE_TYPES){

            $scope.success = null;
            $scope.alerts = [];

            var location = "place-bbsta",
                apiService;

            try {
                // New up our service layer
                apiService = new ApiDataService(true, STORAGE_TYPES.LOCAL);

                // Get Data, straight from cache
                apiService.getAlerts(location).then(function(data){
                    $scope.alerts = data.alert_headers;
                    $scope.success = "Alerts Fetched";
                }, function(msg){
                    $scope.alerts = [];
                    $scope.success = "Fetching Alerts Failed";
                });

                // Get Data, Clear the cache first, and persist new data
                apiService.getFreshAlerts(location).then(function(data){
                    $scope.alerts = data.alert_headers;
                    $scope.success = "Alerts Fetched";
                }, function(msg){
                    $scope.alerts = [];
                    $scope.success = "Fetching Alerts Failed";
                });

            } catch (e) {
                console.log(e.message);
            }

        }]);

})();
