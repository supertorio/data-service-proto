(function(){

    'use strict';

    var app = angular.module('Beam.Data',[]);

    app.constant('STORAGE_TYPES', {
        "SESSION" : 'sessionStorage',
        "LOCAL" : 'localStorage'
    });

    app.factory('Beam.Data.BaseDataService', ['$q', 'STORAGE_TYPES', function($q, STORAGE_TYPES) {

        /**
         * Helper Function takes an array of arguments and
         * concatenates all primitive values with a ';' delimiter
         * @param args
         * @returns {string}
         */
        function stringifyArguments(args) {
            var len = args.length;
            var result = "";

            for (var i= 0; i<len; i++) {
                if (typeof args[i] !== "object") {
                    result += ";"+args[i].toString();
                }
            }

            return result;
        }


        /**
         * Builds a hash signature for a given class/call/argument list
         * @param {string} className
         * @param {string} call
         * @param {Array} args
         * @returns {string} SHA1 Hash Signature
         */
        function getSignatureHash(className, call, args) {
            var signature = className+";"+call+stringifyArguments(args);
            return Sha1.hash(signature);
        }


        /**
         * Custom Exception for Invalid Storage Requests
         * @param message
         * @constructor
         */
        function InvalidStorageException(message){
            this.message = message;
            this.name ="Invalid Storage";
        }


        /**
         * Checks for browser storage capabilities
         * @private
         * @param {string} storageType - name for storage mechanism
         * @returns {boolean}
         */
        function supports(storageType) {
            var key = "foo",
                val = "bar";

            if (storageType === STORAGE_TYPES.SESSION) {
                try {
                    sessionStorage.setItem(key, val);
                    sessionStorage.removeItem(key);
                    return true;
                } catch(e) {
                    console.log(e);
                    return false;
                }
            }
            else if (storageType === STORAGE_TYPES.LOCAL) {
                try {
                    localStorage.setItem(key, val);
                    localStorage.removeItem(key);
                    return true;
                } catch(e) {
                    return false;
                }
            }
            else return false;
        }


        /**
         * Default Constructor
         * @constructor
         */
        var BaseDataService = function(){
            this.className = "";
            this.supportsSessionStorage = supports(STORAGE_TYPES.SESSION);
            this.supportsLocalStorage = supports(STORAGE_TYPES.LOCAL);
            this.useCache = false;
            this.storageMechanism = STORAGE_TYPES.SESSION;
        };


        /**
         * Convenience Method to determine if there is a cached version available
         * @param call - Name of method called
         * @param args - Method 'arguments' property
         * @returns {boolean} true if cached version exists
         */
        BaseDataService.prototype.hasCachedVersion = function(call, args) {
            var callKey = getSignatureHash(this.className, call, args);
            return localStorage.getItem(callKey) !== null;
        };


        /**
         * Fetches Cached Version from local storage
         * @param call - Name of method called
         * @param args - Method 'arguments' property
         * @param fallback - Method to execute if fetching from cache fails
         * @param {boolean} expireFirst - true to clear any cached value and fetch fresh data
         * @returns {promise|*} Deferred object, resolves with stored data
         */
        BaseDataService.prototype.get = function(call, args, fallback, expireFirst) {
            var callKey = getSignatureHash(this.className, call, args),
                dfd     = $q.defer(),
                me      = this,
                cachedData = null;

            if (!angular.isDefined(expireFirst)) expireFirst = false;

            // Not using cache, just pass through to fallback callback
            if (!this.useCache) return fallback();

            // Clear Cache First?
            if (expireFirst) {
                this.expireFromCache(call, args);
                cachedData = null;
            }
            // Otherwise check our storage for a cached value
            else if (this.storageMechanism === STORAGE_TYPES.LOCAL &&
                this.supportsLocalStorage) {
                    cachedData = localStorage.getItem(callKey);
            }
            else if (this.storageMechanism === STORAGE_TYPES.SESSION &&
                this.supportsSessionStorage) {
                    cachedData = sessionStorage.getItem(callKey);
            }

            // Note that if we don't support any storage types, it will
            // fall through with a null value, prompting a fresh fetch
            if (cachedData !== null) {
                // Serve up the cached data wrapped in a promise
                dfd.resolve(JSON.parse(cachedData));
                return dfd.promise;
            } else {
                // Fetch fresh data and persist to cache
                var apiPromise = fallback();
                apiPromise.then(function(data) {
                    me.saveToCache(call, args, data);
                });
                return apiPromise;
            }
        };


        /**
         * Stringifys into JSON and stores data for a call into localStorage
         * @param call - Name of method called
         * @param args - Method 'arguments' property
         * @param data - Data to store
         */
        BaseDataService.prototype.saveToCache = function(call, args, data) {
            var callKey = getSignatureHash(this.className, call, args);
            if (this.storageMechanism === STORAGE_TYPES.LOCAL &&
                this.supportsLocalStorage) {
                    localStorage.setItem(callKey, JSON.stringify(data));
                    return true;
            }
            else if (this.storageMechanism === STORAGE_TYPES.SESSION &&
                this.supportsSessionStorage) {
                    sessionStorage.setItem(callKey, JSON.stringify(data));
                    return true;
            }
            else return false;
        };


        /**
         * Clears the cached value for this call/argument
         * @param call - Name of method called
         * @param args - Method 'arguments' property
         * @returns {boolean} Success?
         */
        BaseDataService.prototype.expireFromCache = function (call, args) {
            var callKey = getSignatureHash(this.className, call, args);
            if (this.storageMechanism === STORAGE_TYPES.LOCAL &&
                this.supportsLocalStorage) {
                    localStorage.removeItem(callKey);
                    return true;
            }
            else if (this.storageMechanism === STORAGE_TYPES.SESSION &&
                this.supportsSessionStorage) {
                    sessionStorage.removeItem(callKey);
                    return true;
            }
            else return false;
        };


        /**
         * Setter for the type of storage to be used for caching (local, session)
         * @throws InvalidStorageException - when an bad storage type is passed in
         * @param storageType
         */
        BaseDataService.prototype.setStorageMechanism = function(storageType) {
            var validMechanism = false;
            for (var i= 0, keys = Object.getOwnPropertyNames(STORAGE_TYPES); i < keys.length; i++) {
                if (STORAGE_TYPES[keys[i]] === storageType) {
                    validMechanism = true;
                    break;
                }
            }

            if (!validMechanism) {
                throw new InvalidStorageException("The storage type you requested is not available");
            }

            this.storageMechanism = storageType;
        };

        // Return the Singleton
        return BaseDataService;

    }]);


    app.factory('ApiDataService',
        ['Beam.Data.BaseDataService', 'Beam.Sample.API',
            function(BaseDataService, SampleApiProvider) {

                var ApiDataService = function(useCache, storageMechanism) {
                    BaseDataService.apply(this, arguments);
                    this.className = "ApiDataService";
                    this.useCache = angular.isDefined(useCache) ? useCache : false;

                    if (angular.isDefined(storageMechanism)) {
                        this.setStorageMechanism(storageMechanism)
                    }
                };

                // reuse the original object prototype
                ApiDataService.prototype = new BaseDataService();


                /**
                 * Sample call which defaults to cached value
                 * @param whichStop
                 * @returns {promise|*}
                 */
                ApiDataService.prototype.getAlerts = function(whichStop) {
                    return this.get('getAlerts', arguments, function(){
                        return SampleApiProvider.getSomeData(whichStop);
                    });
                };


                /**
                 * Sample Call which clears cache and gets fresh values to cache
                 * @param whichStop
                 * @returns {promise|*}
                 */
                ApiDataService.prototype.getFreshAlerts = function(whichStop) {
                    return this.get('getAlerts', arguments, function(){
                        return SampleApiProvider.getSomeData(whichStop);
                    }, true);
                };

                return ApiDataService;
    }]);

})();
