module.exports = function(grunt) {

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// load all grunt tasks matching the `grunt-*` pattern
	require('load-grunt-tasks')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		connect: {
			server: {
				options: {
					base		    : './',
					hostname	  : '0.0.0.0',
					logger		  : 'dev',
					livereload	: true,
					port		    : 9000,
					middleware	: function (connect, options) {

						options.base += '';

						var config = [ // Serve static files.
							connect.static(options.base),
							// Make empty directories browsable.
							connect.directory(options.base)
						];

						return config;
					}
				}
			}
		},

		watch: {

			options: {
				livereload: true
			},

			html: {
				files: ['./index.html']
			},

			js: {
				files: ['./js/**/*.js']
			}
		},

		open: {
			dev: {
				path: 'http://localhost:9000/',
				app: 'Google Chrome'
			}
		}
	});

	// User-configurable browser to launch with (use like grunt --browser="Firefox")
	var browser = grunt.option('browser') || 'Google Chrome';


	// Default task(s).
	grunt.registerTask('default', ['connect', 'open:dev', 'watch']);

};
