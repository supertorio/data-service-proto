(function(){

    'use strict';

    function SampleApiProvider(){

        var host = 'http://realtime.mbta.com/developer/api/v2/';
        var apiKey = 'MTGxs0xtwkWWkDb4Vv1cFg';

        this.$get = ['$http','$q', function($http,$q) {

            this.getSomeData = function(location){

                var dfd = $q.defer();

                function success(response){
                    dfd.resolve(response.data);
                }

                function failure(response) {
                    dfd.reject(response);
                }

                var url = host+'predictionsbystop?stop='+location+'&format=json&api_key='+apiKey;

                $http({
                    method:'GET',
                    url: url,
                    cache: false
                }).then(success,failure);

                // Return DFD
                return dfd.promise;

            };

            return this;
        }];
    }

    angular
        .module('Beam.Sample',[])
        .provider('Beam.Sample.API',SampleApiProvider);


})();